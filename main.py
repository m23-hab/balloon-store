import configparser
import json
import requests
import sqlite3
import threading
import time

from flask import Flask, g
from flask_jsonrpc import JSONRPC

POLL_INTERVAL = 5

app = Flask(__name__)

# CONFIG
config = configparser.ConfigParser(allow_no_value=True)
config.read('config.ini')
print('using upstream: {}'.format('upstream' in config))
allowed_topics = list(map(lambda x: x[0], config.items('topics')))
print(allowed_topics)

# DATABASE INIT
def get_db(init=False):
    if 'db' not in g:
        g.db = sqlite3.connect('database.db')
        if init:
            cursor = g.db.cursor()
            cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
            if len(cursor.fetchall()) == 0:
                cursor.execute('PRAGMA journal_mode=WAL;')
                cursor.execute('CREATE TABLE packet (topic text, seq integer, payload text);')
                g.db.commit()
    return g.db

@app.teardown_appcontext
def teardown_db(*args):
    db = g.pop('db', None)
    if db is not None:
        db.close()

# EXCEPTIONS
class TopicNotAllowedError(Exception):
    def __str__(self): return "Topic not allowed"

# WEB ROUTES
@app.route('/')
def hello_world():
    return 'Hello, World!'
# TODO web view

# API
jsonrpc = JSONRPC(app, '/api')

@jsonrpc.method('getPacket')
def api_get_packet(topic, seq):
    validate(topic, seq)
    cursor = get_db().cursor()
    cursor.execute('SELECT topic,seq,payload FROM packet WHERE topic = ? AND seq = ?', (topic, seq))
    l = unpack(cursor.fetchall())
    return l[0] if len(l) > 0 else None

@jsonrpc.method('getPacketsSince')
def api_get_packets_since(topic, seq):
    validate(topic, seq)
    cursor = get_db().cursor()
    cursor.execute('SELECT topic,seq,payload FROM packet WHERE topic = ? AND seq > ?', (topic, seq))
    return unpack(cursor.fetchall())

@jsonrpc.method('pushPacket')
def api_push_packet(topic, seq, payload):
    validate(topic, seq)
    if topic not in allowed_topics: raise TopicNotAllowedError()
    existing = api_get_packet(topic, seq)
    if existing:
        return False
    else:
        cursor = get_db().cursor()
        cursor.execute('INSERT INTO packet VALUES (?,?,?)', (topic, seq, json.dumps(payload)))
        get_db().commit()
        # TODO push to upstream (if configured)
        return True

@jsonrpc.method('getTopics')
def api_get_topics():
    return allowed_topics

# HELPERS
def unpack(rows):
    return list(map(lambda row: dict(topic=row[0], seq=row[1], payload=json.loads(row[2])), rows))

def validate(topic, seq):
    if not isinstance(topic, str):
        raise TypeError('topic must be string')
    if not isinstance(seq, int):
        raise TypeError('seq must be integer')

# UPSTREAM POLL LOOP
if 'upstream' in config and 'baseurl' in config['upstream']:
    def poll_upstream():
        upstream_api_path = config['upstream']['baseurl'] + '/api'
        while True:
            time.sleep(POLL_INTERVAL)
            with app.app_context():
                reqs = []
                cursor = get_db().cursor()
                for topic in allowed_topics:
                    cursor.execute('SELECT MAX(seq) FROM packet WHERE topic = ?', (topic,))
                    max_seq = cursor.fetchone()[0] or 0
                    reqs.append(dict(jsonrpc='2.0', id=topic, method='getPacketsSince', params=[topic, max_seq]))
                try:
                    r = requests.post(upstream_api_path, data=json.dumps(reqs))
                    parsed = json.loads(r.text)
                    for answer in parsed:
                        if 'id' in answer and answer['id'] and 'result' in answer:
                            for packet in answer['result']:
                                api_push_packet(packet['topic'], packet['seq'], packet['payload'])
                except Exception as e:
                    print('Error polling upstream:', e)
    thread = threading.Thread(target=poll_upstream)
    thread.start()

# MAIN
if __name__ == '__main__':
    with app.app_context():
        get_db(init=True)
    app.run(host='0.0.0.0', port=5000)

